**Worms and Horses — 2020**

In June 2019 took place the workshop “Worms & Horses”: a one-week-long group investigation on the possible bridges between computer viruses and artistic practices. Drawing inspiration from simple associations (e.g. “infinite loop” applied to “book design”) we explored how to transform a priori disruptive behaviours into something inventive and positive.
        <br>
        As part of this, a publication was created and designed in real-time and entirely in the browser, gathering all the produced visual and textual content. Interested in translating the idea of “dormant phase” — keeping a virus inactive for a while and trigger it when certain conditions are fulfilled — we planted in this printed booklet an element that was to be revealed only a year later. We asked the Yi Jing “how could we turn our malicious behaviors into good ones?”. The answer was hidden on a second half of the publication’s cover, which was revealed and interpreted on June 20 2020 at 20:20 (EEST). The whole day was also the occasion to create and share a series of images created to reflect and conclude the last 365 days.
<br>
—
<br>
        The initial workshop was organised by <a href="http://liminal.ro/2019/">Liminal Festival</a>, created by <a href="http://bonjourmonde.net/">Bonjour Monde</a> and hosted by <a href="https://www.oddweb.org/">ODD</a> (Bucharest, Romania). The reveal day was co-organised by <a href="https://mmaintenant.org/index.html">Synesthésie ¬ MMAINTENANT</a> and transmitted during their event Solstices d’été at Zone sensible (Saint-Denis, France).
<br>
—
<br>
![WaH2020](/results.png)
